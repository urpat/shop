+--------+--------------------------------+-----------------------------------------+-------------------------------+------------------------------------------------------------+------------+
| Domain | Method                         | URI                                     | Name                          | Action                                                     | Middleware |
+--------+--------------------------------+-----------------------------------------+-------------------------------+------------------------------------------------------------+------------+
|        | GET|HEAD                       | product                                 | product.index                 | App\Http\Controllers\Product\ProductController@index       | web        |
|        | GET|HEAD                       | product/create                          | product.create                | App\Http\Controllers\Product\ProductController@create      | web        |
|        | GET|HEAD|POST|PUT|PATCH|DELETE | product/{id}/delete                     | product.delete                | App\Http\Controllers\Product\ProductController@delete      | web        |
|        | GET|HEAD                       | product/{id}/edit                       | product.edit                  | App\Http\Controllers\Product\ProductController@edit        | web        |
|        | GET|HEAD                       | product/{id}/product__image             | product.product__image.index  | App\Http\Controllers\Product\ProductImageController@index  | web        |
|        | GET|HEAD                       | product/{id}/product__image/create      | product.product__image.create | App\Http\Controllers\Product\ProductImageController@create | web        |
|        | GET|HEAD|POST|PUT|PATCH|DELETE | product/{id}/product__image/delete/{id} | product.product__image.delete | App\Http\Controllers\Product\ProductImageController@delete | web        |
|        | GET|HEAD                       | product/{id}/product__image/edit/{id}   | product.product__image.edit   | App\Http\Controllers\Product\ProductImageController@edit   | web        |
|        | POST                           | product/{id}/product__image/save        | product.product__image.save   | App\Http\Controllers\Product\ProductImageController@save   | web        |
|        | GET|HEAD|POST|PUT|PATCH|DELETE | product/{id}/product__image/update/{id} | product.product__image.update | App\Http\Controllers\Product\ProductImageController@update | web        |
|        | POST                           | product/{id}/save                       | product.save                  | App\Http\Controllers\Product\ProductController@save        | web        |
|        | GET|HEAD|POST|PUT|PATCH|DELETE | product/{id}/update                     | product.update                | App\Http\Controllers\Product\ProductController@update      | web        |
+--------+--------------------------------+-----------------------------------------+-------------------------------+------------------------------------------------------------+------------+
