<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


// Product

Route::group([
    'prefix' => 'product',
    'as' => 'product.',
    'namespace' => 'Product',
    ], function () {

    Route::get('',                 ['as' => 'index',  'uses' => 'ProductController@index']);

    Route::get('create',           ['as' => 'create', 'uses' => 'ProductController@create']);


    Route::group([
        'prefix' => '{productId}',
        ], function () {

        Route::get('',             ['as' => 'show',  'uses' => 'ProductController@show']);

        Route::post('save',        ['as' => 'save',   'uses' => 'ProductController@save']);

        Route::get('edit',         ['as' => 'edit',   'uses' => 'ProductController@edit']);

        Route::any('update',       ['as' => 'update', 'uses' => 'ProductController@update']);

        Route::any('delete',       ['as' => 'delete', 'uses' => 'ProductController@delete']);


        // ProductImage

        Route::group([
            'prefix' => 'product__image',
            'as' => 'product__image.',
            ], function () {

            Route::get('',             ['as' => 'index',  'uses' => 'ProductImageController@index']);

            Route::get('create',       ['as' => 'create', 'uses' => 'ProductImageController@create']);
            
            Route::post('save',        ['as' => 'save',   'uses' => 'ProductImageController@save']);

            Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductImageController@edit']);

            Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductImageController@update']);

            Route::any('delete/{productImageId}',  ['as' => 'delete', 'uses' => 'ProductImageController@delete']);

        });

    });

});


// User

Route::group([
    'prefix' => 'user',
    'as' => 'user.',
    'namespace' => 'User',
    ], function () {

    Route::get('',         ['as' => 'index',  'uses' => 'UserController@index']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'UserController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'UserController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'UserController@delete']);


    Route::group([
        'prefix' => 'cart',
        'as' => 'cart.'
        ], function () {

        Route::get('',          ['as' => 'index',
                                'uses' => 'UserProductController@index']);

        Route::get('create/{productId}',    ['as' => 'create',
                                'uses' => 'UserProductController@create']);

        Route::any('update/{userProductId}',['as' => 'update',
                                'uses' => 'UserProductController@update']);

        Route::any('delete/{userProductId}',  ['as' => 'delete',
                                   'uses' => 'UserProductController@delete']);

    });

});


// Order

Route::group([
    'prefix' => 'order',
    'as' => 'order.',
    'namespace' => 'Order',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'OrderController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'OrderController@create']);
    Route::post('save/{orderId}',        ['as' => 'save',   'uses' => 'OrderController@save']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'OrderController@delete']);


    Route::group([
        'prefix' => 'order__product',
        'as' => 'order__product.',
        'namespace' => 'Order',
        ], function () {

        Route::get('create',       ['as' => 'create', 'uses' => 'OrderProductController@create']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'OrderProductController@delete']);

    });
});


// Category

Route::group([
    'prefix' => 'category',
    'as' => 'category.',
    'namespace' => 'Category',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'CategoryController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'CategoryController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'CategoryController@save']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'CategoryController@delete']);

});



// Home

Route::get('/', function(){
    return 'Laravel 5.3' . '<br><a href="/product">Продукты</a>';
});



    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm');
    Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');



    // Admin
    Route::get('admin', ['middleware' => 'admin', 'as' => 'admin', function() {

        return view('face.admin');

    }]);