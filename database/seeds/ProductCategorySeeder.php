<?php

use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('product__category')->insert([ //,
                'product_id'         => $i,
                'category_id' => $faker->numberBetween(1,4)
            ]);
        }
    }
}