<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('product')->insert([ //,
                'name'         => $faker->name,
                'description'  => $faker->word,
                'price'        => $faker->numberBetween(1000,20000),
                'availability' => $faker->numberBetween(0,1)
            ]);
        }
    }
}
