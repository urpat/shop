<?php

Route::group([
        'prefix' => 'category',
        'as' => 'category.',
        'namespace' => 'Category',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'CategoryController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'CategoryController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'CategoryController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'CategoryController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'CategoryController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'CategoryController@delete']);

                });

    
        Route::group([
        'prefix' => 'order',
        'as' => 'order.',
        'namespace' => 'Order',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'OrderController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'OrderController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'OrderController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'OrderController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'OrderController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'OrderController@delete']);

                                    include 'Routes/Order/OrderProduct.php';
                            });

    
        Route::group([
        'prefix' => 'product',
        'as' => 'product.',
        'namespace' => 'Product',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'ProductController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'ProductController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'ProductController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductController@delete']);

                                    include 'Routes/Product/ProductCategory.php';
                            include 'Routes/Product/ProductComment.php';
                            include 'Routes/Product/ProductImage.php';
                            include 'Routes/Product/ProductOverview.php';
                            include 'Routes/Product/ProductRating.php';
                            });

    
        Route::group([
        'prefix' => 'user',
        'as' => 'user.',
        'namespace' => 'User',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'UserController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'UserController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'UserController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'UserController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'UserController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'UserController@delete']);

                                    include 'Routes/User/UserProduct.php';
                            });

    