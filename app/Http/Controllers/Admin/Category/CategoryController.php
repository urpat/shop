<?php
namespace App\Http\Controllers\Admin\Category;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Category\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller {

    public function index()
    {
        $category = Category::orderBy('id', 'desc')->paginate(10);

        return view('admin.category.category.index', [
            'category' => $category
        ]);
    }

    public function create()
    {
        return view('admin.category.category.create');
    }

    public function save(Request $request)
    {
        $category = new Category($request->all());

        $category->save();

        return redirect()->route('category.index');
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('admin.category.category.edit', [
            'category' => $category
        ]);
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $category->update($request->all());

        return redirect()->route('category.index');
    }

    public function delete($id)
    {
        $category = Category::where('id', '=', $id);
        $category->delete();

        return redirect()->back();
    }
}