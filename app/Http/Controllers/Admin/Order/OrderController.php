<?php
namespace App\Http\Controllers\Admin\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Order\Order;
use Illuminate\Http\Request;

class OrderController extends Controller {

    public function index()
    {
        $order = Order::orderBy('id', 'desc')->paginate(10);

        return view('admin.order.order.index', [
            'order' => $order
        ]);
    }

    public function create()
    {
        return view('admin.order.order.create');
    }

    public function save(Request $request)
    {
        $order = new Order($request->all());

        $order->save();

        return redirect()->route('order.index');
    }

    public function edit($id)
    {
        $order = Order::find($id);

        return view('admin.order.order.edit', [
            'order' => $order
        ]);
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        $order->update($request->all());

        return redirect()->route('order.index');
    }

    public function delete($id)
    {
        $order = Order::where('id', '=', $id);
        $order->delete();

        return redirect()->back();
    }
}