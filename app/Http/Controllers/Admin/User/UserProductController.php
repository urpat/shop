<?php
namespace App\Http\Controllers\Admin\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\User\UserProduct;
use Illuminate\Http\Request;

class UserProductController extends Controller {

    public function index()
    {
        $userProduct = UserProduct::orderBy('id', 'desc')->paginate(10);

        return view('admin.user.user__product.index', [
            'userProduct' => $userProduct
        ]);
    }

    public function create()
    {
        return view('admin.user.user__product.create');
    }

    public function save(Request $request)
    {
        $userProduct = new UserProduct($request->all());

        $userProduct->save();

        return redirect()->route('userProduct.index');
    }

    public function edit($id)
    {
        $userProduct = UserProduct::find($id);

        return view('admin.user.user__product.edit', [
            'userProduct' => $userProduct
        ]);
    }

    public function update(Request $request, $id)
    {
        $userProduct = UserProduct::find($id);

        $userProduct->update($request->all());

        return redirect()->route('userProduct.index');
    }

    public function delete($id)
    {
        $userProduct = UserProduct::where('id', '=', $id);
        $userProduct->delete();

        return redirect()->back();
    }
}