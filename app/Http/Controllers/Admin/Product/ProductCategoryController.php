<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller {

    public function index()
    {
        $productCategory = ProductCategory::orderBy('id', 'desc')->paginate(10);

        return view('admin.product.product__category.index', [
            'productCategory' => $productCategory
        ]);
    }

    public function create()
    {
        return view('admin.product.product__category.create');
    }

    public function save(Request $request)
    {
        $productCategory = new ProductCategory($request->all());

        $productCategory->save();

        return redirect()->route('productCategory.index');
    }

    public function edit($id)
    {
        $productCategory = ProductCategory::find($id);

        return view('admin.product.product__category.edit', [
            'productCategory' => $productCategory
        ]);
    }

    public function update(Request $request, $id)
    {
        $productCategory = ProductCategory::find($id);

        $productCategory->update($request->all());

        return redirect()->route('productCategory.index');
    }

    public function delete($id)
    {
        $productCategory = ProductCategory::where('id', '=', $id);
        $productCategory->delete();

        return redirect()->back();
    }
}