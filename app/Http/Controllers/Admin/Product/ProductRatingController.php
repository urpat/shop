<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\ProductRating;
use Illuminate\Http\Request;

class ProductRatingController extends Controller {

    public function index()
    {
        $productRating = ProductRating::orderBy('id', 'desc')->paginate(10);

        return view('admin.product.product__rating.index', [
            'productRating' => $productRating
        ]);
    }

    public function create()
    {
        return view('admin.product.product__rating.create');
    }

    public function save(Request $request)
    {
        $productRating = new ProductRating($request->all());

        $productRating->save();

        return redirect()->route('productRating.index');
    }

    public function edit($id)
    {
        $productRating = ProductRating::find($id);

        return view('admin.product.product__rating.edit', [
            'productRating' => $productRating
        ]);
    }

    public function update(Request $request, $id)
    {
        $productRating = ProductRating::find($id);

        $productRating->update($request->all());

        return redirect()->route('productRating.index');
    }

    public function delete($id)
    {
        $productRating = ProductRating::where('id', '=', $id);
        $productRating->delete();

        return redirect()->back();
    }
}