<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\ProductComment;
use Illuminate\Http\Request;

class ProductCommentController extends Controller {

    public function index()
    {
        $productComment = ProductComment::orderBy('id', 'desc')->paginate(10);

        return view('admin.product.product__comment.index', [
            'productComment' => $productComment
        ]);
    }

    public function create()
    {
        return view('admin.product.product__comment.create');
    }

    public function save(Request $request)
    {
        $productComment = new ProductComment($request->all());

        $productComment->save();

        return redirect()->route('productComment.index');
    }

    public function edit($id)
    {
        $productComment = ProductComment::find($id);

        return view('admin.product.product__comment.edit', [
            'productComment' => $productComment
        ]);
    }

    public function update(Request $request, $id)
    {
        $productComment = ProductComment::find($id);

        $productComment->update($request->all());

        return redirect()->route('productComment.index');
    }

    public function delete($id)
    {
        $productComment = ProductComment::where('id', '=', $id);
        $productComment->delete();

        return redirect()->back();
    }
}