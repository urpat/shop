<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\ProductOverview;
use Illuminate\Http\Request;

class ProductOverviewController extends Controller {

    public function index()
    {
        $productOverview = ProductOverview::orderBy('id', 'desc')->paginate(10);

        return view('admin.product.product__overview.index', [
            'productOverview' => $productOverview
        ]);
    }

    public function create()
    {
        return view('admin.product.product__overview.create');
    }

    public function save(Request $request)
    {
        $productOverview = new ProductOverview($request->all());

        $productOverview->save();

        return redirect()->route('productOverview.index');
    }

    public function edit($id)
    {
        $productOverview = ProductOverview::find($id);

        return view('admin.product.product__overview.edit', [
            'productOverview' => $productOverview
        ]);
    }

    public function update(Request $request, $id)
    {
        $productOverview = ProductOverview::find($id);

        $productOverview->update($request->all());

        return redirect()->route('productOverview.index');
    }

    public function delete($id)
    {
        $productOverview = ProductOverview::where('id', '=', $id);
        $productOverview->delete();

        return redirect()->back();
    }
}