<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\ProductImage;
use Illuminate\Http\Request;

class ProductImageController extends Controller {

    public function index()
    {
        $productImage = ProductImage::orderBy('id', 'desc')->paginate(10);

        return view('admin.product.product__image.index', [
            'productImage' => $productImage
        ]);
    }

    public function create()
    {
        return view('admin.product.product__image.create');
    }

    public function save(Request $request)
    {
        $productImage = new ProductImage($request->all());

        $productImage->save();

        return redirect()->route('productImage.index');
    }

    public function edit($id)
    {
        $productImage = ProductImage::find($id);

        return view('admin.product.product__image.edit', [
            'productImage' => $productImage
        ]);
    }

    public function update(Request $request, $id)
    {
        $productImage = ProductImage::find($id);

        $productImage->update($request->all());

        return redirect()->route('productImage.index');
    }

    public function delete($id)
    {
        $productImage = ProductImage::where('id', '=', $id);
        $productImage->delete();

        return redirect()->back();
    }
}