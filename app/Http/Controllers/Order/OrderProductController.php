<?php
namespace App\Http\Controllers\Admin\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Order\OrderProduct;
use Illuminate\Http\Request;

class OrderProductController extends Controller {

    public function index()
    {
        $orderProduct = OrderProduct::orderBy('id', 'desc')->paginate(10);

        return view('admin.order.order__product.index', [
            'orderProduct' => $orderProduct
        ]);
    }

    public function create()
    {
        return view('admin.order.order__product.create');
    }

    public function save(Request $request)
    {
        $orderProduct = new OrderProduct($request->all());

        $orderProduct->save();

        return redirect()->route('orderProduct.index');
    }

    public function edit($id)
    {
        $orderProduct = OrderProduct::find($id);

        return view('admin.order.order__product.edit', [
            'orderProduct' => $orderProduct
        ]);
    }

    public function update(Request $request, $id)
    {
        $orderProduct = OrderProduct::find($id);

        $orderProduct->update($request->all());

        return redirect()->route('orderProduct.index');
    }

    public function delete($id)
    {
        $orderProduct = OrderProduct::where('id', '=', $id);
        $orderProduct->delete();

        return redirect()->back();
    }
}