<?php
namespace App\Http\Controllers\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Order\Order;
use App\Model\Order\OrderProduct;
use App\Model\User\UserProduct;
use App\Model\User\User;
use Illuminate\Http\Request;

class OrderController extends Controller {

    public function index()
    {
        $order = Order::orderBy('id', 'desc')->paginate(10);

        return view('face.order.index', [
            'order' => $order
        ]);
    }

    public function create(Request $request)
    {
        $order = new Order();

        $order->user_id = $request->user()->id;

        $order->save();

        $userProductList = UserProduct::where('user_id', '=', $order->user_id)->get();

        foreach ($userProductList as $userProduct)
        {
            $orderProduct = new OrderProduct();

            $orderProduct->product_id = $userProduct->product_id;

            $orderProduct->order_id = $order->id;

            $orderProduct->save();
        }

        $orderProductList = OrderProduct::where('order_id', '=', $order->id)
            ->get();


        return view('face.order.edit', [
            'orderProductList' => $orderProductList,
            'order' => $order
            ]);
    }

    public function save(Request $request, $orderId)
    {
        $order = Order::find($orderId);

        $order->amount = $request->amount;

        $order->total = $request->total;

        $userProduct = UserProduct::where('user_id', '=', $request->user()->id);

        $userProduct->delete();

        $order->save();

        return redirect()->route('order.index');
    }

    public function edit($id)
    {
        $order = Order::find($id);

        return view('face.order.edit', [
            'order' => $order
        ]);
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        $order->update($request->all());

        return redirect()->route('order.index');
    }

    public function delete($id)
    {
        $order = Order::where('id', '=', $id);
        $order->delete();

        return redirect()->back();
    }
}