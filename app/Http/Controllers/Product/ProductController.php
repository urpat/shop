<?php
namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Product\Product;
use App\Model\Product\ProductCategory;
use App\Model\Product\ProductImage;
use Illuminate\Http\Request;
use App\Model\Category\Category;


class ProductController extends Controller {

    public function index(Request $request)
    {
        $query = Product::with('product__category.category');

        $query->filter($request->filter);

        $query->sort($request->sort);


        $category = Category::all();

        return view('face.index', [
            'product' => $query->paginate(6),
            'category' => $category
        ]);
    }

    public function create()
    {
        $product = new Product;

        $product->save();

        $productId = $product->id;

        $category = Category::all();

        return view('face.product.create', [
            'productId' => $productId,
            'category'  => $category
        ]);
    }

    public function save(Request $request)
    {
        $product = Product::find($request->productId);

        $product->name = $request->name;

        $product->save();

        $productCategory = new ProductCategory;

        $productCategory->product_id = $product->id;

        $productCategory->category_id = $request->category;

        $productCategory->save();

        return redirect()->route('product.edit', $product->id);
    }

    public function edit($id)
    {
        $category = Category::all();

        $product = Product::find($id);

        $productImages = $product->image()->get();

        return view('face.product.edit', [
            'category' => $category,
            'product' => $product,
            'productImages' => $product->image()->get()
        ]);
    }

    public function update(Request $request, $productId)
    {

        $product = Product::find($productId);

        $product->description = $request->description;

        $product->price = $request->price;

        if ($request->availability) {
            $availability = 1;
        } else {
            $availability = 0;
        };

        $product->availability = $availability;

        $product->save();


        $category = Category::where('name', $request->category)->get();

        foreach ($request->category as $categoryId) {

            $productCategoryId = ProductCategory::where('product_id', '=', $product->id)->get();

            foreach ($productCategoryId as $productCategory)
            {
                $productCategory->category_id = $categoryId;
            }

        }


        return redirect()->back();
    }

    public function delete($id)
    {
        $product = Product::where('id', '=', $id);

        $foreignKeyOff = DB::select('SET foreign_key_checks = 0');

        $product->delete();

        $foreignKeyOn = DB::select('SET foreign_key_checks = 1');

        return redirect()->route('product.index');
    }

    public function show($productId)
    {
        $category = Category::all();

        $product = Product::find($productId);

        $productImages = $product->image()->get();


        foreach ($product->product__category as $productCategory)
        {
            $productCategoryName[] = $productCategory->category->name;
        }


        return view('face.product.show', [
            'category' => $category,
            'productCategory' => $productCategoryName,
            'product' => $product,
            'productImages' => $product->image()->get()
        ]);
    }

}