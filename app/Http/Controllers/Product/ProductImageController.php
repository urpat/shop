<?php
namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\ProductImage;
use Illuminate\Http\Request;
use App\Model\Product\Product;
use Intervention\Image\ImageManagerStatic as Image;


class ProductImageController extends Controller {

    public function index()
    {

        return view('face.product__image.index', [
            'productImage' => $productImage
        ]);
    }

    public function create()
    {
        return view('face.product__image.create');
    }

    public function save(Request $request)
    {
        $manager = new \Intervention\Image\ImageManager(array('driver' => 'gd'));

        foreach ($request->file('image') as $file)
        {
            $productImage = new ProductImage();

            $productImage->product_id = $request->productId;
            $productImage->name = $file->hashName();

            $originalSrc = $productImage->src('original', true);

            $file->move(dirname($originalSrc), basename($originalSrc));

            foreach (config('thumbnail') as $dir => $size)
            {
                $tumbnailSrc = $productImage->src($dir, true);

                if (!is_dir(dirname($tumbnailSrc))) {
                    mkdir(dirname($tumbnailSrc));
                }

                $imgSrc = $manager->make($originalSrc)
                    ->resize($size['height'], $size['width'])
                ->save($tumbnailSrc);

                //echo '<img src="' . $productImage->src($dir, false) . '" />'; die;
            }

            $productImage->save();
        }

        return redirect()->back();
    }

    public function edit($productId, $id)
    {
        $productImage = ProductImage::find($id);

dd(
    $productImage->src('original'),
    $productImage->src('original', true)
);

        return view('admin.product.product__image.edit', [
            'productImage' => $productImage
        ]);
    }

    public function update(Request $request, $id)
    {
        $productImage = ProductImage::find($id);

        $productImage->update($request->all());

        return redirect()->route('productImage.index');
    }

    public function delete(Request $request)
    {
        $productImage = ProductImage::where('id', '=', $request->productImageId);
        $productImage->delete();

        return redirect()->back();
    }
}