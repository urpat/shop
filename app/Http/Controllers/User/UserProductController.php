<?php
namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\User\UserProduct;
use Illuminate\Http\Request;

class UserProductController extends Controller {

    public function index(Request $request)
    {
        $userProductList = UserProduct::where('user_id', '=', $request->user()->id)->get();


        return view('face.user.user__product.index', [
            'userProductList' => $userProductList
        ]);
    }

    public function create(Request $request, $productId)
    {
        $productCart = UserProduct::where('user_id', '=', $request->user()->id)->where('product_id', '=', $productId)->get();

        if ($productCart->count() > 0)
        {
            foreach ($productCart as $productCart)
            {
                $productCart->quantity = $productCart->quantity + 1;
                $productCart->save();     
            }

        } 

        else {

        $userProduct = new UserProduct();

        $userProduct->user_id = $request->user()->id;

        $userProduct->product_id = $productId;

        $userProduct->save();

        }

        return redirect()->back();
    }

    public function save(Request $request)
    {
        $userProduct = new UserProduct($request->all());

        $userProduct->save();

        return redirect()->route('userProduct.index');
    }

    public function edit($id)
    {
        $userProduct = UserProduct::find($id);

        return view('admin.user.user__product.edit', [
            'userProduct' => $userProduct
        ]);
    }

    public function update(Request $request, $userProductId)
    {
        $userProduct = UserProduct::find($userProductId);        
        $userProduct->quantity = $request->quantity;
        $userProduct->save();

        return redirect()->back();
    }

    public function delete($userProductId)
    {
        $userProduct = UserProduct::where('id', '=', $userProductId);
        $userProduct->delete();

        return redirect()->back();
    }
}