<?php
namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\User\User;
use Illuminate\Http\Request;

class UserController extends Controller {

    public function index()
    {
        $user = User::find($id);

        return view('face.user.index', [
            'user' => $user
        ]);
    }

    public function create()
    {
        return view('admin.user.user.create');
    }

    public function save(Request $request)
    {
        $user = new User($request->all());

        $user->save();

        return redirect()->route('user.index');
    }

    public function edit(Request $request, $id)
    {
        $user = User::find($id);

        return view('face.user.index', [
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->update($request->all());

        return redirect()->back();
    }

    public function delete($id)
    {
        $user = User::where('id', '=', $id);
        $user->delete();

        return redirect('/product');
    }
}