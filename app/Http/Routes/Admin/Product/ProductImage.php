<?php
Route::group([
    'prefix' => 'product__image',
    'as' => 'product__image.',
    'namespace' => 'Product',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ProductImageController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductImageController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductImageController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductImageController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductImageController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductImageController@delete']);

});