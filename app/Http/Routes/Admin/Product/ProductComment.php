<?php
Route::group([
    'prefix' => 'product__comment',
    'as' => 'product__comment.',
    'namespace' => 'Product',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ProductCommentController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductCommentController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductCommentController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductCommentController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductCommentController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductCommentController@delete']);

});