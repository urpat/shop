<?php
Route::group([
    'prefix' => 'product__category',
    'as' => 'product__category.',
    'namespace' => 'Product',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ProductCategoryController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductCategoryController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductCategoryController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductCategoryController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductCategoryController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductCategoryController@delete']);

});