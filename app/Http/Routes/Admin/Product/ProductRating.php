<?php
Route::group([
    'prefix' => 'product__rating',
    'as' => 'product__rating.',
    'namespace' => 'Product',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ProductRatingController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductRatingController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductRatingController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductRatingController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductRatingController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductRatingController@delete']);

});