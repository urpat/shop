<?php
Route::group([
    'prefix' => 'product__overview',
    'as' => 'product__overview.',
    'namespace' => 'Product',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ProductOverviewController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductOverviewController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductOverviewController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductOverviewController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductOverviewController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductOverviewController@delete']);

});