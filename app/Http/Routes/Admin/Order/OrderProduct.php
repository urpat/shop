<?php
Route::group([
    'prefix' => 'order__product',
    'as' => 'order__product.',
    'namespace' => 'Order',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'OrderProductController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'OrderProductController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'OrderProductController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'OrderProductController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'OrderProductController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'OrderProductController@delete']);

});