<?php
Route::group([
    'prefix' => 'user__product',
    'as' => 'user__product.',
    'namespace' => 'User',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'UserProductController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'UserProductController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'UserProductController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'UserProductController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'UserProductController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'UserProductController@delete']);

});