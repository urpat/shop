<?php
Route::group([
    'prefix' => 'category',
    'as' => 'category.',
    'namespace' => 'Category',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'CategoryController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'CategoryController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'CategoryController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'CategoryController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'CategoryController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'CategoryController@delete']);

});