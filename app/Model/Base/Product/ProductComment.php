<?php
namespace App\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    public $timestamps = false;

    protected $table = 'product__comment';

    protected $fillable = [
        'product_id',
        'comment',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product\Product', 'product_id', 'id');
    }
}