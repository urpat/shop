<?php
namespace App\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    public $timestamps = false;

    protected $table = 'product__category';

    protected $fillable = [
        'product_id',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo('App\Model\Category\Category', 'category_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product\Product', 'product_id', 'id');
    }
}