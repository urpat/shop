<?php
namespace App\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    protected $table = 'product';

    protected $fillable = [
        'name',
        'description',
        'price',
        'availability',
    ];

    public function image()
    {
        return $this->hasMany('App\Model\Product\ProductImage', 'product_id', 'id');
    }

    public function comment()
    {
        return $this->hasMany('App\Model\Product\ProductComment', 'product_id', 'id');
    }

    public function product__category()
    {
        return $this->hasMany('App\Model\Product\ProductCategory', 'product_id', 'id');
    }

    public function overview()
    {
        return $this->hasMany('App\Model\Product\ProductOverview', 'product_id', 'id');
    }

    public function rating()
    {
        return $this->hasMany('App\Model\Product\ProductRating', 'product_id', 'id');
    }

    public function order_product()
    {
        return $this->hasMany('App\Model\Order\OrderProduct', 'product_id', 'id');
    }

    public function user_product()
    {
        return $this->hasMany('App\Model\User\UserProduct', 'product_id', 'id');
    }
}