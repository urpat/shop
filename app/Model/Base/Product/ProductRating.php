<?php
namespace App\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;

class ProductRating extends Model
{
    public $timestamps = false;

    protected $table = 'product__rating';

    protected $fillable = [
        'product_id',
        'rating',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product\Product', 'product_id', 'id');
    }
}