<?php
namespace App\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;

class ProductOverview extends Model
{
    public $timestamps = false;

    protected $table = 'product__overview';

    protected $fillable = [
        'product_id',
        'text',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product\Product', 'product_id', 'id');
    }
}