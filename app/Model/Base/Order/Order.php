<?php
namespace App\Model\Base\Order;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = true;

    protected $table = 'order';

    protected $fillable = [
        'user_id',
        'amount',
        'total',
    ];

    public function order_product()
    {
        return $this->hasMany('App\Model\Order\OrderProduct', 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User\User', 'user_id', 'id');
    }
}