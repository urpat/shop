<?php
namespace App\Model\Base\User;

use Illuminate\Foundation\Auth\User as Model;

class User extends Model
{
    public $timestamps = false;

    protected $table = 'user';

    protected $fillable = [
        'name',
        'password',
        'email',
        'address',
        'city',
    ];

    public function user_product()
    {
        return $this->hasMany('App\Model\User\UserProduct', 'user_id', 'id');
    }

    public function order()
    {
        return $this->hasMany('App\Model\Order\Order', 'user_id', 'id');
    }
}