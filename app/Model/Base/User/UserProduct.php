<?php
namespace App\Model\Base\User;

use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
    public $timestamps = false;

    protected $table = 'user__product';

    protected $fillable = [
        'user_id',
        'product_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User\User', 'user_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product\Product', 'product_id', 'id');
    }
}