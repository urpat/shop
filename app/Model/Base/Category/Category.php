<?php
namespace App\Model\Base\Category;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    protected $table = 'category';

    protected $fillable = [
        'name',
    ];

    public function product()
    {
        return $this->hasMany('App\Model\Product\ProductCategory', 'category_id', 'id');
    }
}