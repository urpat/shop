<?php
namespace App\Model\User;

use App\Model\Base\User\UserProduct as BaseUserProduct;

class UserProduct extends BaseUserProduct
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addUserProductFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = UserProduct::noFilter($query);
        $query = UserProduct::addUserProductFilter($query, $params);

        return $query;
    }
}