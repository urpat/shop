<?php
namespace App\Model\Category;

use App\Model\Base\Category\Category as BaseCategory;

class Category extends BaseCategory
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addCategoryFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = Category::noFilter($query);
        $query = Category::addCategoryFilter($query, $params);

        return $query;
    }
}