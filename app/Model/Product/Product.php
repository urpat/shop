<?php

namespace App\Model\Product;

use App\Model\Base\Product\Product as BaseProduct;

class Product extends BaseProduct
{
    static $listFields = [];

    static $rules = [];

    public function scopeFilter($query, $filter)
    {
        if (isset($filter['category']) && !empty($filter['category']))
        {

            $query
                ->Join('product__category', 'product.id', 'product__category.product_id')
                ->where('product__category.category_id', '=', $filter['category']);

        }

        if (isset($filter['priceMinimum']) && !empty($filter['priceMinimum']))
        {
            $query->where('price', '>=', (int) $filter['priceMinimum']);
        }

        if (isset($filter['priceMaximum']) && !empty($filter['priceMaximum']))
        {
            $query->where('price', '<=', (int) $filter['priceMaximum']);
        }

        // Наличие продуктов
        // if (!empty($filter['availabilityNo']) && !empty($filter['availabilityYes']))
        // {
            
        // }

        //     elseif (isset($filter['availabilityYes']) && !empty($filter['availabilityYes']))
        //     {
        //         $query->where('availability', '=', '1');
        //     }

        //         elseif (isset($filter['availabilityNo']) && !empty($filter['availabilityNo']))
        //         {
        //             $query->where('availability', '=', '0');
        //         }

        return $query;
    }

    public function scopeSort($query, $sort)
    {
        if (isset($sort['field']) && !empty($sort['field']))
        {
                if ($sort['order'] == 'asc')
                {
                    $query->oldest($sort['field']);
                }

                elseif ($sort['order'] == 'desc')
                {
                    $query->latest($sort['field']);
                }

                if ($sort['order'] == 'true')
                {
                    $query->where('availability', '>', '0');
                }

                if ($sort['order'] == 'false')
                {
                    $query->where('availability', '<', '1');
                }

        }

        return $query;
    }

}