<?php
namespace App\Model\Product;

use App\Model\Base\Product\ProductComment as BaseProductComment;

class ProductComment extends BaseProductComment
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addProductCommentFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = ProductComment::noFilter($query);
        $query = ProductComment::addProductCommentFilter($query, $params);

        return $query;
    }
}