<?php
namespace App\Model\Product;

use App\Model\Base\Product\ProductImage as BaseProductImage;

class ProductImage extends BaseProductImage
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addProductImageFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = ProductImage::noFilter($query);
        $query = ProductImage::addProductImageFilter($query, $params);

        return $query;
    }

    public function src($dir, $absolute = false)
    {
        $src = [];

        $src[] = $this->product_id;

        $src[] = $dir;

        $src[] = $this->name;

        return (($absolute) ? public_path() : '') . '/image/product/' . implode('/', $src);
    }

    public function resize($dir, $absolute = false)
    {

    }
}