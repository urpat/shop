<?php
namespace App\Model\Product;

use App\Model\Base\Product\ProductRating as BaseProductRating;

class ProductRating extends BaseProductRating
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addProductRatingFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = ProductRating::noFilter($query);
        $query = ProductRating::addProductRatingFilter($query, $params);

        return $query;
    }
}