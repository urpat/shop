<?php
namespace App\Model\Product;

use App\Model\Base\Product\ProductOverview as BaseProductOverview;

class ProductOverview extends BaseProductOverview
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addProductOverviewFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = ProductOverview::noFilter($query);
        $query = ProductOverview::addProductOverviewFilter($query, $params);

        return $query;
    }
}