<?php
namespace App\Model\Product;

use App\Model\Base\Product\ProductCategory as BaseProductCategory;

class ProductCategory extends BaseProductCategory
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addProductCategoryFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = ProductCategory::noFilter($query);
        $query = ProductCategory::addProductCategoryFilter($query, $params);

        return $query;
    }

    public function scopeFilterCategory($queryCategory, $filter)
    {
        if (isset($filter['category']) && !empty($filter['category']))
        {
            $queryCategory->where('category_id', '=', (int) $filter['category']);
        }

        return $queryCategory;
    }
    
} 