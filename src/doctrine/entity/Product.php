<?php



/**
 * Product
 */
class Product
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var boolean
     */
    private $availability;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set availability
     *
     * @param boolean $availability
     *
     * @return Product
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * Get availability
     *
     * @return boolean
     */
    public function getAvailability()
    {
        return $this->availability;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $image;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $category;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $overview;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rating;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
        $this->overview = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rating = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add image
     *
     * @param \ProductImage $image
     *
     * @return Product
     */
    public function addImage(\ProductImage $image)
    {
        $this->image[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \ProductImage $image
     */
    public function removeImage(\ProductImage $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add comment
     *
     * @param \ProductComment $comment
     *
     * @return Product
     */
    public function addComment(\ProductComment $comment)
    {
        $this->comment[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \ProductComment $comment
     */
    public function removeComment(\ProductComment $comment)
    {
        $this->comment->removeElement($comment);
    }

    /**
     * Get comment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add category
     *
     * @param \ProductCategory $category
     *
     * @return Product
     */
    public function addCategory(\ProductCategory $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \ProductCategory $category
     */
    public function removeCategory(\ProductCategory $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add overview
     *
     * @param \ProductOverview $overview
     *
     * @return Product
     */
    public function addOverview(\ProductOverview $overview)
    {
        $this->overview[] = $overview;

        return $this;
    }

    /**
     * Remove overview
     *
     * @param \ProductOverview $overview
     */
    public function removeOverview(\ProductOverview $overview)
    {
        $this->overview->removeElement($overview);
    }

    /**
     * Get overview
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOverview()
    {
        return $this->overview;
    }

    /**
     * Add rating
     *
     * @param \ProductRating $rating
     *
     * @return Product
     */
    public function addRating(\ProductRating $rating)
    {
        $this->rating[] = $rating;

        return $this;
    }

    /**
     * Remove rating
     *
     * @param \ProductRating $rating
     */
    public function removeRating(\ProductRating $rating)
    {
        $this->rating->removeElement($rating);
    }

    /**
     * Get rating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRating()
    {
        return $this->rating;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $order_product;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user_product;


    /**
     * Add orderProduct
     *
     * @param \OrderProduct $orderProduct
     *
     * @return Product
     */
    public function addOrderProduct(\OrderProduct $orderProduct)
    {
        $this->order_product[] = $orderProduct;

        return $this;
    }

    /**
     * Remove orderProduct
     *
     * @param \OrderProduct $orderProduct
     */
    public function removeOrderProduct(\OrderProduct $orderProduct)
    {
        $this->order_product->removeElement($orderProduct);
    }

    /**
     * Get orderProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderProduct()
    {
        return $this->order_product;
    }

    /**
     * Add userProduct
     *
     * @param \UserProduct $userProduct
     *
     * @return Product
     */
    public function addUserProduct(\UserProduct $userProduct)
    {
        $this->user_product[] = $userProduct;

        return $this;
    }

    /**
     * Remove userProduct
     *
     * @param \UserProduct $userProduct
     */
    public function removeUserProduct(\UserProduct $userProduct)
    {
        $this->user_product->removeElement($userProduct);
    }

    /**
     * Get userProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserProduct()
    {
        return $this->user_product;
    }
}
