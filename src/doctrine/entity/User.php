<?php



/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user_product;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $order;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user_product = new \Doctrine\Common\Collections\ArrayCollection();
        $this->order = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userProduct
     *
     * @param \UserProduct $userProduct
     *
     * @return User
     */
    public function addUserProduct(\UserProduct $userProduct)
    {
        $this->user_product[] = $userProduct;

        return $this;
    }

    /**
     * Remove userProduct
     *
     * @param \UserProduct $userProduct
     */
    public function removeUserProduct(\UserProduct $userProduct)
    {
        $this->user_product->removeElement($userProduct);
    }

    /**
     * Get userProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserProduct()
    {
        return $this->user_product;
    }

    /**
     * Add order
     *
     * @param \Order $order
     *
     * @return User
     */
    public function addOrder(\Order $order)
    {
        $this->order[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \Order $order
     */
    public function removeOrder(\Order $order)
    {
        $this->order->removeElement($order);
    }

    /**
     * Get order
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrder()
    {
        return $this->order;
    }
}
