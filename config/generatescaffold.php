<?php

return [

// Основные сущности
    'masters' =>
    [
        'Category',
        'Order',
        'Product',
        'User',
    ],

    // Подчененные сущности
    'subordinates' =>
    [
        'Order' =>
        [
            'OrderProduct',
        ],
        'Product' =>
        [
            'ProductCategory',
            'ProductComment',
            'ProductImage',
            'ProductOverview',
            'ProductRating',
        ],
        'User' =>
        [
            'UserProduct',
        ],
    ],
    'options' =>
    [

    ],


];