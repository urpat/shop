@extends('layouts.master')


@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

                <div class="panel-heading">


                        <div class="panel-heading">
                        	<h3 class="text">Панель управления</h3>
                        </div>


                        <table class="table">

                            <tr>

                                <td>

                                    <a href="{{ route('product.create') }}" class="btn btn-primary" />
                                        Добавить продукт
                                    </a>
                                    
                                    <a class="btn btn-warning" href="{{ route('product.index') }}">
                                        Список продуктов
                                    </a>

                                </td>

                                <td>

                                    <a href="{{ route('category.create') }}" class="btn btn-primary" />
                                        Добавить категорию
                                    </a>

                                    <a class="btn btn-warning" href="{{ route('category.index') }}">
                                        Список категорий
                                    </a>

                                </td>

                            </tr>

                        </table>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection