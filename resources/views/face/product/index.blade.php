<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

				<div class="panel-heading">   
					<h1 class="text-center">Интернет-магазин</h1>
				</div>

				@include('face.product.filter')

				<div class="panel-heading">

					<div class="panel-heading">

						@include('face.product.list')
						
					</div>

				</div>


            </div>
        </div>
    </div>
</div>
