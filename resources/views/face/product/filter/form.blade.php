@php $filter = app('request')->get('filter') @endphp

<form action="{{ route('product.index') }}" method="get">
	
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <input type="hidden" name="filter[category]" value="{{ $filter['category'] }}">

	<label for="price">Стоимость</label>
	      от <input type="text" name="filter[priceMinimum]" value="{{

	      $filter['priceMinimum'] or old('filter[priceMinimum]')

	       }}" />
	      до <input type="text" name="filter[priceMaximum]" value="{{

	      $filter['priceMaximum'] or old('filter[priceMaximum]') }}" />


{{--		<br><br>

        <label for="availabilityYes">В наличии</label><input type="checkbox" name="filter[availabilityYes]" value="{{ $filter['availabilityYes'] or old('filter[priceMaximum]') }}"/>

        &ensp;&ensp;

        <label for="availabilityNo">Нет в наличии</label><input type="checkbox" name="filter[availabilityNo]" />
--}}

		<hr>

		<div class="form-group">

		    <label for="category">Категория: </label>

					@foreach ($category as $category)

						<a href="{{ route('product.index', [
							'filter' => ['category' => $category->id]
						]) }}">

							@if ($category->id == $filter['category'])

								<i><b>{{ $category->name }}</b></i>

							@else

								{{ $category->name }}

							@endif
						
						</a>

						&ensp;&ensp;

					@endforeach

		</div>

		<button class="btn btn-primary" type="submit">Отфильтровать!</button>

</form>