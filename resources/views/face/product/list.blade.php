<table class="table">

	<tr>

		<td>id<br>

			<a href="{{ route('product.index', [
				'filter' => app('request')->filter,
				'sort'   => ['field' => 'id', 'order' => 'asc']
			]) }}" />▲</a>

			<a href="{{ route('product.index', [
				'filter'   => app('request')->filter,
				'sort'     => ['field' => 'id', 'order' => 'desc']
			]) }}" />▼</a>

		</td>

		<td>Название<br>

			<a href="{{ route('product.index', [
				'filter' => app('request')->filter,
				'sort'   => ['field' => 'name', 'order' => 'asc']
			]) }}" />▲</a>

			<a href="{{ route('product.index', [
				'filter'   => app('request')->filter,
				'sort'     => ['field' => 'name', 'order' => 'desc']
			]) }}" />▼</a>

		</td>

		<td>Цена<br>

			<a href="{{ route('product.index', [
				'filter' => app('request')->filter,
				'sort'   => ['field' => 'price', 'order' => 'asc']
			]) }}" />▲</a>

			<a href="{{ route('product.index', [
				'filter'   => app('request')->filter,
				'sort'     => ['field' => 'price', 'order' => 'desc']
			]) }}" />▼</a>

		</td>

		<td>Категория</td>

		<td>Наличие<br>

			<a href="{{ route('product.index', [
				'filter' => app('request')->filter,
				'sort'   => ['field' => 'availability', 'order' => 'true']
			]) }}" />⊙</a>

			<a href="{{ route('product.index', [
				'filter'   => app('request')->filter,
				'sort'     => ['field' => 'availability', 'order' => 'false']
			]) }}" />⊗</a>

		</td>

		<td>Действие</td>

	</tr>


	@foreach ($product as $item)
		<tr>
			@include('face.product.list.item')
		</tr>
	@endforeach


</table>


{{ $product->appends([
	'filter' => app('request')->filter,
	'sort' => ['field'    => app('request')->sort['field'],
			   'order'    => app('request')->sort['order']]
	])
->links() }}
