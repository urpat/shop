@extends('layouts.master')


@section('content')

<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

				<div class="panel-default">
					<div class="panel-heading">

						<h2 class="text">Добавить продукт</h2>


						<form action="{{ route('product.save', $productId) }}" method="POST" id="id-form_messages" >

						    <input type="hidden" name="_token" value="{{ csrf_token() }}">

						        <div class="form-group">
						            <label for="name">Название: </label>
						            <input class="form-control" placeholder="Пример: Samsung G55 mini" name="name" type="text" id="name">
						        </div>

						        <select name="category">

							        @foreach ($category as $key => $category)
									    				    		
								    		<option value="{{ $category->id }}">
								    			{{ $category->name }}
								    		</option>

							        @endforeach

							    </select>

							    <hr>

						        <div class="form-group">
						            <input class="btn btn-primary" type="submit" value="Продолжить">
						        </div>

						</form>

					</div>

				</div>

            </div>

        </div>

    </div>

</div>

@endsection