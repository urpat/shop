<div class="form-group">

    <label for="category">Категория: </label>

    <br>

    <select name="category[]" multiple>

        @foreach ($category as $key => $category)

		    @foreach ($product->product__category as $productCategory)

		    	@if ($category->id == $productCategory->category_id)
		    		
		    		<option selected value="{{ $category->id }}">{{ $category->name }}</option>

		    	@else

		    		<option value="{{ $category->id }}">{{ $category->name }}</option>

		    	@endif

		    @endforeach

        @endforeach

    </select>

</div>