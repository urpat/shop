<td>{{ $item->id }}</td>
<td><a href="{{ route('product.show', $item->id) }}">{{ $item->name }}</a></td>
<td>{{ $item->price }}</td>

<td>@foreach($item->product__category as $productCategory)
		{{ $productCategory->category->id . ':' . $productCategory->category->name }}	
	@endforeach
</td>

<td>{{ $item->availability ? 'ДА' : 'НЕТ' }}</td>

<td>
	@include('face.product.list.item_action')
</td>