@if (Auth::check())

	<a href="{{ route('user.cart.create', $item->id) }}" class="btn btn-primary">В корзину</a>

	@if (Auth::user()->role == 'admin')

		<hr>

		<a href="{{ route('product.edit', $item->id) }}" class="btn btn-primary">edit</a>
		<a href="{{ route('product.delete', $item->id) }}" class="btn btn-danger">delete</a>

	@endif

@endif
