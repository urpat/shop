@if ($productImages->count())

    @foreach ($productImages as $productImage)

        <div>

        	<img src="{{ $productImage->src('200x200') }}">

            <br>

	        	<a class="btn btn-danger" href="{{ route('product.product__image.delete', ['productId' => $product->id, 'productImageId' => $productImage->id]) }}">
	        		Удалить
	        	</a>

        </div>
    <br>
    @endforeach

@else
    <center>Изображений нет</center>
@endif