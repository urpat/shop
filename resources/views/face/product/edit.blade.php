@extends('layouts.master')


@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

                <div class="panel-heading">

                    <h1 class="text-center">
                        id: {{ $id = $product->id }} / {{ $product->name }}
                    </h1>

                </div>

                <div class="panel-heading">
                    
                    <div class="panel panel-default">

                        <div class="panel-heading">

                            <h4 class="text">Отредактируйте товар</h4>

                                <hr/>

                                @include('face.product.__form.edit')

                        </div>

                    </div>

                    <hr>

                    @include('face.product__image.create')
                    
                    @include('face.product.list.image200x200')

                </div>

            </div>

        </div>

    </div>

</div>

@endsection