@extends('layouts.master')


@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

                <div class="panel-heading">

                @foreach ($productCategory as $categoryName)

                            <h4 class="text">Категория: {{ $categoryName }}</h4>

                @endforeach

                    <table class="table">

                        <tr>

                            <td>id</td>

                            <td>Название</td>

                            <td>Цена</td>

                            <td>В наличие</td>

                            <td>Действие</td>

                        </tr>

                        <tr>

                            <td>{{ $product->id }}</td>

                            <td>{{ $product->name }}</td>

                            <td>{{ $product->price }}</td>

                            <td>{{ $product->availability ? 'ДА' : 'НЕТ' }}</td>

                            <td>

                            <a href="{{ route('user.cart.create', $product->id) }}" 
                                    class="btn btn-primary">В корзину</a>

                            <a href="" class="btn btn-success">Купить</a>

                            <hr>

                            <a href="{{ route('product.edit', $product->id) }}" class="btn btn-primary">edit</a>

                            <a href="{{ route('product.delete', $product->id) }}" class="btn btn-danger">delete</a>

                            </td>

                        </tr>

                    </table>

                    <hr>

                    {{ $product->description }}

                    <hr>
              
                    @include('face.product.list.image200x200')

                </div>

            </div>

        </div>

    </div>

</div>

@endsection