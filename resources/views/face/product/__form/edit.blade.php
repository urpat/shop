<form method="POST" id="id-form_messages" action="{{ route('product.update', $id) }}">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    @include('face.product.list.category')

    <div class="form-group">
        <label for="description">Описание: </label>

        <br>
        <textarea name="description" id="description" rows="4" cols="70" >{{ $product->description }}</textarea>

    </div>

    <div class="form-group">
        <label for="price">Стоимость: </label>
        <input class="form-control" placeholder="" name="price" type="text" id="price" 
        value="{{ $product->price }}">
    </div>

    <div class="form-group">

        <label for="availability">В наличии: </label>
        
        @if ($product->availability == 1)
            <input name="availability" type="checkbox" id="availability" checked/>
        @else
            <input name="availability" type="checkbox" id="availability" >
        @endif

    </div>

    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Отправить">
    </div>

</form>