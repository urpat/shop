@extends('layouts.master')


@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

                <div class="panel-heading">

                    <form action="{{ route('order.save', $order->id) }}" method="post">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="panel-heading">
                        	<h3 class="text">Оформление заказа</h3>
                        </div>


                        <table class="table">

                            <tr>

                                <td>id</td>

                                <td>Название</td>

                                <td>Цена</td>

                                <td>Изображение</td>

                                <td>В наличие</td>

                                <td>Количество</td>

                            </tr>

                            @foreach ($orderProductList as $orderProduct)

                            <tr>

                                <td>{{ $orderProduct->product->id }}</td>

                                <td>{{ $orderProduct->product->name }}</td>

                                <td>{{ $price = $orderProduct->product->price }}</td>

                                <td>

                                @if ($orderProduct->product->image()->get()->count())

                                	@foreach ($orderProduct->product->image()->take(1)->get() as $orderProductImage)

                                		<img src="{{ $orderProductImage->src('100x100') }}">

                                	@endforeach

                                @else

                                	Нет изображения

                                @endif                        

                                </td>

                                <td>
                                    {{ $orderProduct->product->availability >= 1 ? 'Да' : 'Нет' }}
                                </td>

                                <td>
                                    @php
                                        $quantity = $orderProduct->product
                                                    ->user_product()->get();
                                    @endphp

                                        {{ $quantity = $quantity[0]->quantity }}
                                </td>

                            </tr>

                            @php 
                            	$total[] = $price * $quantity;
                            	$quantityAll[] = $quantity;
                            @endphp

                            @endforeach

                        </table>

                        <hr>
                        <label for="quantityAll">Количество продуктов заказа: {{ $amount = array_sum($quantityAll) }}</label>

                        <input type="hidden" name="amount" value="{{ $amount }}">
                        
                        <br>
                        <label for="total">Сумма заказа: {{ $total = array_sum($total) }}</label>

                        <input type="hidden" name="total" value="{{ $total }}">      

                        <hr>


                        Город: 
                        {{ Auth::user()->city }}
                        <br>
                        Адрес: 
                        {{ Auth::user()->address }}

                        <hr>
                        <input type="submit" class="btn btn-primary" value="Подтвердить заказ">

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection