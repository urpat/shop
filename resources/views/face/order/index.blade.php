@extends('layouts.master')


@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

                <div class="panel-heading">

                    <div class="panel-heading">
                    	<h3 class="text">Ваши заказы</h3>
                    </div>


                    <table class="table">

                        <tr>

                            <td>id</td>

                            <td>Количество продуков</td>

                            <td>Сумма заказа</td>

                            <td>Дата заказа</td>

                        </tr>

                        @foreach ($order as $order)

                        <tr>

                            <td>{{ $order->id }}</td>

                            <td>{{ $order->amount }}</td>

                            <td>{{ $order->total }}</td>

                            <td>{{ $order->created_at }}</td>

                        </tr>

                        @endforeach

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection