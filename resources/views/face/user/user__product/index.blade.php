@extends('layouts.master')


@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

                <div class="panel-heading">

                    <div class="panel-heading">
                    	<h3 class="text">Корзина продуктов</h3>
                    </div>


                    <table class="table">

                        <tr>

                            <td>id</td>

                            <td>Название</td>

                            <td>Цена</td>

                            <td>Изображение</td>

                            <td>В наличие</td>

                            <td>Количество</td>

                            <td>Действие</td>

                        </tr>

                        @foreach ($userProductList as $userProduct)

                        <tr>

                            <td>{{ $userProduct->product->id }}</td>

                            <td>{{ $userProduct->product->name }}</td>

                            <td>{{ $price = $userProduct->product->price }}</td>

                            <td>

                            @if ($userProduct->product->image()->get()->count())

                            	@foreach ($userProduct->product->image()->take(1)->get() as $productImage)

                            		<img src="{{ $productImage->src('100x100') }}">

                            	@endforeach

                            @else

                            	Нет изображения

                            @endif                        

                            </td>

                            <td>{{ $userProduct->product->availability >= 1 ? 'Да' : 'Нет' }}</td>

                            <td>

                            <form action="{{ route('user.cart.update', $userProduct->id) }}" method="post>"

	                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

	                            <input type="text" size="7" name="quantity" 
	                            	value="{{ $quantity = $userProduct->quantity }}">

	                            <input class="btn btn-primary" type="submit" value="Обновить">

	                        </form>

                            </td>

                            <td>
                            	<a href="{{ route('user.cart.delete', $userProduct->id) }}" class="btn btn-danger">Удалить</a>
                            </td>

                        </tr>

                        @php
                        	$total[] = $price * $quantity;
                        	$quantityAll[] = $quantity;
                        @endphp

                        @endforeach

                    </table>

                    <hr>

                    <label for="quantityAll">Количество продуктов в корзине: 
                        @if (isset($quantityAll))
                            {{ array_sum($quantityAll) }}
                        @else
                            0
                        @endif
                    </label>
                    
                    <br>
                    <label for="total">Сумма продуктов: 
                        @if (isset($total))
                            {{ array_sum($total) }}
                        @else
                            0
                        @endif
                    </label>

                    <hr>
                        
                        @if (isset($total))
                            <a href="{{ route('order.create') }}" class="btn btn-primary">Оформить заказ</a>
                        @else                            
                        @endif             

                </div>

            </div>

        </div>

    </div>

</div>

@endsection