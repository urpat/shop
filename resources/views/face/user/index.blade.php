@extends('layouts.master')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

				<div class="panel-heading">   
					<h2 class="text-center">Профиль</h2>
				</div>

				<div class="panel-heading">

					<div class="panel-heading">

					<label for="login">Логин: </label>
						{{ $user->name }}

						<hr>

					<label for="email">Email: </label>
						{{ $user->email }}

						<hr>

					<label for="created_at">Дата регистрации: </label>
						{{ $user->created_at }}
						
						<form action="{{ route('user.update', Request::route('id')) }}" method="get">

						    <input type="hidden" name="_token" value="{{ csrf_token() }}">

						    <hr>

							<label for="city">Город: </label>
							      <input type="text" name="city" value="{{ $user->city }}" />
							
							<hr>

							<label for="address">Адрес: </label>
							      <input type="text" name="address" value="{{ $user->address }}"/>

							<hr>
						
							<button class="btn btn-primary" type="submit">Обновить</button>

								<a class="btn btn-danger" href="{{ route('user.delete', Request::route('id')) }}">Удалить аккаунт</a>

						</form>

					</div>

				</div>


            </div>
        </div>
    </div>
</div>

@endsection



