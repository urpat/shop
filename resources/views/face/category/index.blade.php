@extends('layouts.master')


@section('content')

<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

				<div class="panel-default">

					<div class="panel-heading">

						<h2 class="text">Список категорий</h2>

							<table class="table">

								@foreach ($category as $category)

										<tr>
											<td>
												{{ $category->name }}
											</td>
											<td>
												<a class="btn btn-danger" href="{{ route('category.delete', $category->id) }}">Удалить</a>
											</td>
										</tr>

								@endforeach

							</table>		

					</div>

				</div>

            </div>

        </div>

    </div>

</div>

@endsection