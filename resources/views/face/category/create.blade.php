@extends('layouts.master')


@section('content')

<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

				<div class="panel-default">
					<div class="panel-heading">

						<h2 class="text">Добавить категорию</h2>


						<form action="{{ route('category.save') }}" method="POST" id="id-form_messages" >

						    <input type="hidden" name="_token" value="{{ csrf_token() }}">

						        <div class="form-group">
						            <label for="name">Название: </label>
						            <input class="form-control" name="name" type="text" id="name">
						        </div>


						        <div class="form-group">
						            <input class="btn btn-primary" type="submit" value="Сохранить">
						        </div>

						</form>

					</div>

				</div>

            </div>

        </div>

    </div>

</div>

@endsection